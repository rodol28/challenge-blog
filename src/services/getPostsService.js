import { API_URL } from "./settings";

export default function getPostsServices() {
  return fetch(`${API_URL}`)
    .then(response => response.json())
    .then(data => data)
    .catch(err => {
      console.error(err);
      return [];
    });
}
