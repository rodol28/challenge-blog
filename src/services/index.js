import getPostsService from "./getPostsService";
import getSinglePostService from "./getSinglePostService";
import savePostService from "./savePostService";
import updatePostService from "./updatePostService";
import deletePostService from "./deletePostService";

export {
  getPostsService,
  getSinglePostService,
  savePostService,
  updatePostService,
  deletePostService,
};
