import { API_URL } from "./settings";

export default function deletePostService({ idPost }) {
  return fetch(`${API_URL}/${idPost}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return {};
    });
}
