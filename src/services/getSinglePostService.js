import { API_URL } from "./settings";

export default function getSinglePostService({ idPost }) {
  return fetch(`${API_URL}/${idPost}`)
    .then(response => response.json())
    .then(data => data)
    .catch(err => {
      console.error(err);
      return {};
    });
}
