import { API_URL } from "./settings";

export default function savePostService({ title, content }) {
  const post = { title, body: content };

  return fetch(`${API_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(post),
  })
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return {};
    });
}
