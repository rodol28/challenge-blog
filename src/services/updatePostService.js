import { API_URL } from "./settings";

export default function updatePostService({ idPost, title, content }) {
  const post = { title, body: content };

  return fetch(`${API_URL}/${idPost}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(post),
  })
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return {};
    });
}
