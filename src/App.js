import "./App.css";

import Routes from "components/Routes";
import Navbar from "components/Navbar";
import { AppContextProvider } from "context/AppContext";

function App() {
  return (
    <>
      <AppContextProvider>
        <div className="App">
          <Navbar />
          <Routes />
        </div>
      </AppContextProvider>
    </>
  );
}

export default App;
