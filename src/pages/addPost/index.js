import React from "react";
import FormPost from "components/FormPost";
import Spinner from "components/Spinner";
import styles from "./AddPost.module.css";
import { useSavePost } from "hooks";

export default function AddPost() {

  const {
    title,
    content,
    isSubmiting,
    handleChange,
    handleSubmit,
  } = useSavePost();

  return (
    <>
      <FormPost
        handleSubmit={handleSubmit}
        handleChange={handleChange}
        title={title}
        content={content}
        isSubmiting={isSubmiting}
        pageTitle={"Agregar un Post"}
        btnDescription={"Publicar"}
      />

      {isSubmiting ? (
        <div className={styles.spinner_submiting}>
          <Spinner />
        </div>
      ) : null}
    </>
  );
}
