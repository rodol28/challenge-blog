import React from "react";
import FormPost from "components/FormPost";
import { useEditPost, useSinglePost } from "hooks";
import Spinner from "components/Spinner";
import PostNotFound from "components/PostNotFound";
import styles from "./EditPost.module.css";

export default function EditPost({ params }) {
  const { id } = params;

  const {
    title,
    content,
    isLoading,
    isSubmiting,
    handleChange,
    handleSubmit,
  } = useEditPost({
    idPost: id,
  });

  const { isPostEmpty } = useSinglePost({ idPost: id });

  return (
    <>
      {isLoading ? (
        <div className={styles.spinner_loading}>
          <Spinner />
        </div>
      ) : isPostEmpty ? (
        <PostNotFound />
      ) : (
        <FormPost
          handleSubmit={handleSubmit}
          handleChange={handleChange}
          title={title}
          content={content}
          isSubmiting={isSubmiting}
          pageTitle={"Editar el Post"}
          btnDescription={"Actualizar"}
        />
      )}
      {isSubmiting ? (
        <div className={styles.spinner_submiting}>
          <Spinner />
        </div>
      ) : null}
    </>
  );
}
