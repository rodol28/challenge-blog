import React from "react";
import { usePost } from "hooks";
import ListOfPosts from "components/ListOfPosts";
import Spinner from "components/Spinner";
import styles from "./Home.module.css";

export default function Home() {
  const { posts, setPosts, isLoading } = usePost();

  return (
    <>
      <h2 className={styles.page_title}>Lista de Posts</h2>
      {isLoading ? (
        <div className={styles.spinner_loading}>
          <Spinner />
        </div>
      ) : (
        posts && <ListOfPosts posts={posts} setPosts={setPosts} />
      )}
    </>
  );
}
