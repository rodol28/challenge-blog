import React from "react";
import { useSinglePost } from "hooks";
import styles from "./Detail.module.css";

import Spinner from "components/Spinner";
import PostNotFound from "components/PostNotFound";

export default function Detail({ params }) {
  const { id } = params;
  const { post, isLoading, isPostEmpty } = useSinglePost({ idPost: id });

  return (
    <>
      {isLoading ? (
        <div className={styles.spinner_loading}>
          <Spinner />
        </div>
      ) : (
        <div className={styles.container_post}>
          {post && (
            <>
              <h3 className={styles.title_post}>{post.title}</h3>
              <p className={styles.body_post}>{post.body}</p>
            </>
          )}

          {isPostEmpty ? (
            <>
              <PostNotFound />
            </>
          ) : null}
        </div>
      )}
    </>
  );
}
