import { useContext } from "react";
import AppContext from "context/AppContext";

export function useAppContext() {
  const { postSelected, setPostSelected } = useContext(AppContext);
  
  return {
    postSelected,
    setPostSelected,
  };
}
