import { useState, useEffect } from "react";
import { getSinglePostService } from "services";
import { useLocation } from "wouter";
import { updatePostService } from "services";

export function useEditPost({ idPost }) {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmiting, setIsSubmiting] = useState(false);
  
  // eslint-disable-next-line
  const [_, pushLocation] = useLocation();

  useEffect(() => {
    setIsLoading(true);
    getSinglePostService({ idPost })
    .then(post => {
      setTitle(post.title);
      setContent(post.body);
      setIsLoading(false);
    });
  }, [idPost]);

  const handleChange = e => {
    const { name, value } = e.target;
    if (name === "title") setTitle(value);
    if (name === "content") setContent(value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setIsSubmiting(true);
    updatePostService({ idPost, title, content })
    .then(post => {
      setIsSubmiting(false);
      pushLocation("/");
      console.log("Post updated", post);
    });
  };

  return {
    title,
    content,
    isLoading,
    isSubmiting,
    handleChange,
    handleSubmit
  };
}
