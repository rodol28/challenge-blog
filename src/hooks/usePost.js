import { useState, useEffect } from "react";
import { getPostsService } from "services";

export function usePost() {
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getPostsService()
    .then(posts => {
      setPosts(posts);
      setIsLoading(false);
    });
  }, []);

  return { posts, setPosts, isLoading };
}
