import { useState } from "react";
import { useLocation } from "wouter";
import { savePostService } from "services";

export function useSavePost() {
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [isSubmiting, setIsSubmiting] = useState(false);
  
    // eslint-disable-next-line
    const [_, pushLocation] = useLocation();
  
    const handleChange = e => {
      const { name, value } = e.target;
      if (name === "title") setTitle(value);
      if (name === "content") setContent(value);
    };
  
    const handleSubmit = e => {
      e.preventDefault();
      setIsSubmiting(true);
      savePostService({ title, content }).then(post => {
        setIsSubmiting(false);
        pushLocation("/");
        console.log("Post saved", post);
      });
    };

  return {
    handleSubmit,
    handleChange,
    title,
    content,
    isSubmiting,
  }
}