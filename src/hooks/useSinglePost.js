import { useState, useEffect } from "react";
import { getSinglePostService } from "services";
import { useAppContext } from "hooks";

export function useSinglePost({ idPost }) {
  const [post, setPost] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isPostEmpty, setIsPostEmpty] = useState(false);

  const { setPostSelected } = useAppContext();

  useEffect(() => {
    setIsLoading(true);
    getSinglePostService({ idPost }).then(post => {
      setIsLoading(false);
      setPost(post);
      setIsPostEmpty(Object.keys(post).length === 0);
      setPostSelected(idPost);
    });
  }, [idPost, setPostSelected]);

  return {
    post,
    isLoading,
    isPostEmpty,
  };
}
