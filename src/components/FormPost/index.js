import React from "react";
import styles from "./FormPost.module.css";

export default function FormPost({
  handleSubmit,
  handleChange,
  title,
  content,
  isSubmiting,
  pageTitle,
  btnDescription,
}) {
  return (
    <div className={styles.container_form}>
      <h2 className={styles.page_title}>{pageTitle}</h2>
      <form onSubmit={handleSubmit} className={styles.form}>
        <input
          onChange={handleChange}
          value={title || ""}
          className={styles.input_title}
          type="text"
          name="title"
          placeholder="Titulo..."
          autoComplete="off"
          required
        />
        <textarea
          onChange={handleChange}
          value={content || ""}
          className={styles.input_content}
          name="content"
          rows="10"
          placeholder="Contenido..."
          autoComplete="off"
          required
        ></textarea>
        <div>
          <button className={styles.btn_post} disabled={isSubmiting}>
            {btnDescription}
          </button>
        </div>
      </form>
    </div>
  );
}
