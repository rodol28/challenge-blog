import React from "react";
import { Link } from "wouter";
import styles from "./PostNotFound.module.css";

export default function PostNotFound() {
  return (
    <>
      <h3 className={styles.title_post}>¡El post no existe!</h3>
      <Link to="/" className={styles.backToHome}>
        ← Volver al home
      </Link>
    </>
  );
}
