import React from "react";
import styles from "./Navbar.module.css";
import Burger from "./Burger";
import { Link } from "wouter";

export default function Navbar() {
  return (
    <nav className={styles.navbar}>
      <div className={styles.logo}>
        <Link to="/" className={`${styles.link_logo}`}>Blog</Link>
      </div>
      <Burger />
    </nav>
  );
}
