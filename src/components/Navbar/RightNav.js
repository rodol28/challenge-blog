import React from "react";
import styled from "styled-components";
import { Link, useRoute } from "wouter";
import { useAppContext } from "hooks";

const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  padding-left: 0;

  li {
    padding: 18px 10px;
  }

  li a {
    text-decoration: none;
    font-size: 1.4rem;
    color: #000;
  }

  @media (max-width: 768px) {
    opacity: ${({ open }) => (open ? "1" : "0")};
    flex-flow: column nowrap;
    margin-top: auto;
    background: #0d0d0d;
    position: fixed;
    top: 0;
    right: 0;
    height: 100vh;
    width: 300px;
    padding-top: 3.5rem;
    display: flex;
    transform: ${({ open }) => (open ? "translateX(0);" : "translateX(100%)")};
    transition: transform 0.3s ease-out;
    z-index: 99;

    li {
      font-size: 24px;
      color: #ffff;
      text-align: center;
    }

    li a {
      text-decoration: none;
      font-size: 1.4rem;
      color: #fff;
    }
  }
`;

export default function RightNav({ open }) {
  // eslint-disable-next-line
  const [match, _] = useRoute("/post/detail/:id");
  const { postSelected } = useAppContext();

  const postSelectedURL = `/post/edit/${postSelected}`;

  return (
    <Ul open={open}>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/post/add">Agregar Post</Link>
      </li>
      {match && (
        <li>
          <Link to={postSelectedURL}>Editar Post</Link>
        </li>
      )}
    </Ul>
  );
}
