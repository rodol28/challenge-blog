import React from "react";
import styles from "./NotFound404.module.css";

export default function NotFound404() {
  return (
    <div className={styles.not_found_container}>
      <h3 className={styles.not_found_title}>Oops!</h3>
      <h3 className={styles.not_found_title}>Page not found | 404</h3>
    </div>
  );
}
