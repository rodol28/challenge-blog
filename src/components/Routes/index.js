import React from "react";
import { Route, Switch } from "wouter";

import Home from "pages/home";
import AddPost from "pages/addPost";
import Detail from "pages/detail";
import EditPost from "pages/editPost";
import NotFound404 from 'components/NotFound404'

export default function Routes() {
  return (
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/post/add" component={AddPost} exact />
      <Route path="/post/detail/:id" component={Detail} exact />
      <Route path="/post/edit/:id" component={EditPost} exact />
      <Route><NotFound404/></Route>
    </Switch>
  );
}
