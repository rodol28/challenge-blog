import React from "react";
import styles from "./ListOfPosts.module.css";
import { Link } from "wouter";
import { deletePostService } from "services";

export default function ListOfPosts({ posts, setPosts }) {
  const handleDelete = idPost => {
    if (window.confirm("¿Estas seguro de que quieres borrarlo?")) {
      deletePostService({ idPost }).then(res => {
        const filteredPost = posts.filter(post => {
          return post.id !== idPost;
        });
        setPosts(filteredPost);
        console.log("Post deleted");
      });
    }
  };

  return (
    <ul className={styles.list_content_ul}>
      {posts.map(({ id, title }) => {
        return (
          <li key={id} className={styles.post_li}>
            <Link
              to={`/post/edit/${id}`}
              className={`${styles.btn_edit} fas fa-edit`}
            ></Link>
            <button
              onClick={() => handleDelete(id)}
              className={styles.btn_delete}
            >
              <i className="fas fa-trash-alt"></i>
            </button>
            <p className={styles.post_title_p}>{title}</p>
            <p className={styles.container_see_more}>
              <Link to={`/post/detail/${id}`} className={styles.see_more}>
                <i className="fas fa-eye"/> Ver más
              </Link>
            </p>
          </li>
        );
      })}
    </ul>
  );
}
