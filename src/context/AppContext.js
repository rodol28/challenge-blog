import React, { useState } from "react";

const Context = React.createContext({});

export function AppContextProvider({ children }) {
  const [postSelected, setPostSelected] = useState(-1);

  return (
    <Context.Provider value={{ postSelected, setPostSelected }}>
      {children}
    </Context.Provider>
  );
}

export default Context;
